# Surve✌🏽

Welcome to **Surve✌🏽**, a modern way to take surveys in a cool fashion!

## Introduction

"Surve✌🏽" brings a fresh and engaging approach to online surveys. It's designed to make the experience of creating and participating in surveys both fun and intuitive.

### Features

- **User-Friendly Interface:** Enjoy a clean and straightforward experience whether you're setting up a survey or responding to one.
- **Interactive Questions:** Engage with a variety of question types that make every survey an interesting experience.
- **Instant Feedback:** Get immediate insights with real-time results and analytics.