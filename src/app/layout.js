import { Inter } from "next/font/google";
import MainNavbar from "./components/MainNavbar";
import Footer from "./components/Footer";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Surve✌🏽",
  description: "A modern way to create surveys",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className="grid grid-rows-[auto,1fr,auto] min-h-screen">
        <MainNavbar />
        <main className={`${inter.className} container mx-auto p-4`} >
          {children}
        </main>
        {/* <Footer/> */}
      </body>
    </html>
  );
}
