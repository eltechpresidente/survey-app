import ProgressBar from "./ProgressBar";

const ProgressBarMain = ({
  currentQuestionIndexProgressBar,
  totalQuestions,
  difficulty,
}) => {
  const progressPercentage =
    (currentQuestionIndexProgressBar / totalQuestions) * 100;

  return (
    <div className="mb-4">
      <ProgressBar progress={progressPercentage} difficulty={difficulty} />
    </div>
  );
};

export default ProgressBarMain;
