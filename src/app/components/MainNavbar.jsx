"use client";
import Link from "next/link";
import Image from "next/image";
import hclogo from "../../Images/logo.png";
import { usePathname } from "next/navigation";

const MainNavbar = () => {
  const pathname = usePathname();
  const isSurveyPage = pathname.startsWith("/survey");

  if (isSurveyPage) {
    return null;
  }

  return (
<nav className="bg-white border-b-2 border-black p-2">
    <div className="container mx-auto flex justify-center">
      <Link href="/">
        <Image 
          src={hclogo} 
          alt="HablaCode Logo" 
          width={40} 
          height={40} 
          className="h-10 w-auto" 
          priority 
        />
      </Link>
    </div>
  </nav>

  );
};

export default MainNavbar;
