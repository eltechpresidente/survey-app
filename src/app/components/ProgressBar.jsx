import { FaTimes } from "react-icons/fa";
import Link from "next/link";

const ProgressBar = ({ progress }) => {
  const barStyle = {
    width: `${progress}%`,
    backgroundColor: "rgb(34, 197, 94)",
  };

  return (
    <div className="flex items-center">
      <Link className="mr-2" href="/">
        <FaTimes className="text-lg text-gray-600" />
      </Link>
      <div className="bg-gray-200 h-4 w-full rounded-full overflow-hidden">
        <div className="h-full" style={barStyle}></div>
      </div>
    </div>
  );
};

export default ProgressBar;
