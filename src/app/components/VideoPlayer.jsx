import React from 'react';

const VideoPlayer = () => {
    const videoUrl = "https://survey-voice-recording.s3.amazonaws.com/promo-1080p.mp4";

    return (
        <video className="w-full md:max-w-lg h-auto rounded-lg border-2 border-black  shadow-lg mt-4" controls playsInline webkit-playsinline="true">
            <source src={videoUrl} type="video/mp4" />
            Your browser does not support the video tag.
        </video>
    );
};

export default VideoPlayer;
