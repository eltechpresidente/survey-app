"use client";
import { usePathname } from "next/navigation";

export default function Footer() {
  const pathname = usePathname();
  const isSurveyPage = pathname.startsWith("/survey");

  return (
    <>
      {isSurveyPage ? null : (
        <footer className="bg-white border-t-2 border-black text-center p-4 text-xs">
          2024
        </footer>
      )}
    </>
  );
}
