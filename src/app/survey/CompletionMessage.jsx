import Link from "next/link";

export const CompletionMessage = ({ difficulty, firstQuestionId }) => (
  <div className="container mx-auto p-4 m-4 text-center">
    <h1 className="text-2xl md:text-3xl font-bold">
      Thank you for completing our technical {difficulty} course!
    </h1>
    <h1>🎉🎉🎉</h1>
    <div className="mt-6">
      <Link href="/">
        <button className="bg-black text-white font-bold py-2 px-4 mr-6 rounded-md border-2 mt-4 w-full md:w-auto hover:border-black hover:text-black hover:bg-white">
          Go Back Home
        </button>
      </Link>
      <Link href={`/survey/${difficulty}/${firstQuestionId}`}>
        <button className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded mt-4 w-full md:w-auto">
          Retake Assessment
        </button>
      </Link>
    </div>
  </div>
);
