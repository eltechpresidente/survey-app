"use client";
import { useState, useEffect, useMemo, useRef } from "react";
import {
  FaPlayCircle,
  FaPauseCircle,
  FaArrowLeft,
  FaArrowRight,
} from "react-icons/fa";
import { usePathname, useRouter } from "next/navigation";
import ProgressBarMain from "@/app/components/ProgressBarMain";
import { correctMessages, incorrectMessages } from "../../feedback";
import { QuestionNotFound } from "../../QuestionNotFound";
import { CompletionMessage } from "../../CompletionMessage";
import questions from "./questions.json";
import VideoPlayer from "@/app/components/VideoPlayer";
import Link from "next/link";
import "../../style.css";

const QuestionsPage = () => {
  const pathname = usePathname();
  const router = useRouter();
  const audioRef = useRef(null);

  const pathSegments = pathname.split("/");
  const difficulty = pathSegments[pathSegments.length - 2];
  const id = pathSegments[pathSegments.length - 1];
  const questionId = parseInt(id);

  const questionsIndexed = useMemo(
    () =>
      questions.reduce((acc, question) => {
        acc[question.difficulty] = acc[question.difficulty] || [];
        acc[question.difficulty].push(question);
        return acc;
      }, {}),
    [questions]
  );

  const filteredQuestions = questionsIndexed[difficulty] || [];
  const question = filteredQuestions.find((q) => q.id === questionId);
  const totalQuestions = questionsIndexed[difficulty]?.length || 0;
  const currentQuestionIndexProgressBar = filteredQuestions.findIndex(
    (q) => q.id === questionId
  );
  const currentQuestionIndex = filteredQuestions.findIndex(
    (q) => q.id === questionId
  );
  const firstQuestionId = questionsIndexed[difficulty]?.[0]?.id || 1;

  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [isCorrect, setIsCorrect] = useState(null);
  const [showCompletionMessage, setShowCompletionMessage] = useState(false);
  const isLastQuestion = currentQuestionIndex === filteredQuestions.length - 1;
  const [disableAnswers, setDisabledAnswers] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [currentCaption, setCurrentCaption] = useState("");
  const [feedbackMessage, setFeedbackMessage] = useState("");
  const [showCaptions, setShowCaptions] = useState(false);

  const togglePlayPause = () => {
    if (isPlaying || (selectedAnswer && isCorrect)) {
      if (!isPlaying) {
        audioRef.current.play();
        setIsPlaying(true);
      } else {
        audioRef.current.pause();
        setIsPlaying(false);
      }
    }
  };

  useEffect(() => {
    if (isCorrect && question) {
      setDisabledAnswers(true);
      setShowCaptions(true);

      if (question.audio) {
        setIsPlaying(true);
        audioRef.current.src = question.audio;
        audioRef.current.play();
        audioRef.current.onended = () => {
          setIsPlaying(false);
        };
        setCurrentCaption(question.captions);
      } else {
        setCurrentCaption(question.captions);
      }
    }
  }, [
    isCorrect,
    currentQuestionIndex,
    router,
    difficulty,
    filteredQuestions,
    isLastQuestion,
    question?.audio,
    question?.captions,
  ]);

  const handleChoice = (choice) => {
    const correct = choice === question.answer;
    setSelectedAnswer(choice);
    setIsCorrect(correct);

    if (correct) {
      setDisabledAnswers(true);
      setFeedbackMessage(
        correctMessages[Math.floor(Math.random() * correctMessages.length)]
      );
    } else {
      setFeedbackMessage(
        incorrectMessages[Math.floor(Math.random() * correctMessages.length)]
      );
    }
  };

  if (!question) {
    return <QuestionNotFound />;
  }

  if (showCompletionMessage) {
    return (
      <CompletionMessage
        difficulty={difficulty}
        firstQuestionId={firstQuestionId}
      />
    );
  }

  return (
    <div>
      <ProgressBarMain
        currentQuestionIndexProgressBar={currentQuestionIndexProgressBar}
        totalQuestions={totalQuestions}
        difficulty={difficulty}
      />
      <div className="mt-6">
        <h1 className="md:text-3xl text-xl font-bold">{question.question}</h1>
        <div className="mt-4">
          {question.audio && (
            <div>
              <button
                onClick={togglePlayPause}
                className="text-3xl"
                disabled={!isPlaying && (!selectedAnswer || !isCorrect)}
              >
                {isPlaying ? (
                  <FaPlayCircle className="text-green-500" />
                ) : (
                  <FaPauseCircle className="text-red-500" />
                )}
              </button>
              <audio
                ref={audioRef}
                onPlay={() => setIsPlaying(true)}
                onPause={() => setIsPlaying(false)}
              />
            </div>
          )}
        </div>
        {selectedAnswer && (
          <div
            className={`mt-4 md:text-md text-sm ${
              isCorrect ? "text-green-600" : "text-red-600"
            }`}
          >
            {feedbackMessage}
            {isCorrect && question.captions && (
              <p
                className={`captions mt-2 text-black md:text-sm text-md ${
                  showCaptions ? "show" : ""
                }`}
              >
                {currentCaption || question.captions}
              </p>
            )}
          </div>
        )}
        <div className="mt-4">
          {["A", "B", "C", "D"].map((choice) => (
            <button
              key={choice}
              onClick={() => handleChoice(choice)}
              disabled={disableAnswers}
              className={`block w-full text-left mt-4 text-md md:p-4 p-3 rounded-md border-2 border-black ${
                selectedAnswer === choice
                  ? isCorrect
                    ? "bg-green-500 text-white"
                    : "bg-red-500 text-white"
                  : "bg-gray-200 text-black hover:bg-gray-300"
              }`}
            >
              {choice}: {question[choice]}
            </button>
          ))}
        </div>
        <div className="flex justify-between items-center  mt-6">
          {currentQuestionIndex > 0 && (
            <Link href={`/survey/${difficulty}/${questionId - 1}`}>
              <button className="text-gray-600">
                <FaArrowLeft className="text-lg" />
              </button>
            </Link>
          )}
          {isCorrect && currentQuestionIndex >= 0 &&
            currentQuestionIndex < filteredQuestions.length - 1 && (
              <Link
                href={`/survey/${difficulty}/${
                  filteredQuestions[currentQuestionIndex + 1].id
                }`}
              >
                <button className="text-gray-600">
                  <FaArrowRight className="text-lg" />
                </button>
              </Link>
            )}
          {isLastQuestion && isCorrect && !showCompletionMessage && (
            <button
              onClick={() => setShowCompletionMessage(true)}
              className="bg-white text-black font-bold p-2 rounded-md border-2 border-black hover:bg-gray-100"
            >
              Complete
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default QuestionsPage;
