export const correctMessages = [
  "Brilliant! You nailed it!",
  "Correct! Keep up the great work!",
  "Super, that's correct!",
  "Exactly right, well done!",
  "Impressive, keep it up!"
];

export const incorrectMessages = [
  "Oops, close but not quite. Try again!",
  "Almost had it! You're getting there!",
  "Not this time, but keep trying!",
  "A miss, but you're learning!",
  "Close, but not quite there!"
];