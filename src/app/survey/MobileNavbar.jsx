import { useState, useEffect, useRef } from "react";
import Link from "next/link";

const MobileNavbar = ({ filteredQuestions, currentQuestionId, difficulty }) => {
  const [isOpen, setIsOpen] = useState(false);
  const navbarRef = useRef(null);

  const toggleNavbar = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (navbarRef.current && !navbarRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [navbarRef]);

  return (
    <div
      className="fixed bottom-0 left-0 right-0 bg-white z-50 rounded-t-lg border-2 border-black"
      ref={navbarRef}
    >
      <div
        className={`p-2 transition-height ease-in-out duration-300 text-sm ${
          isOpen ? "h-32" : "h-10"
        }`}
      >
        <button
          onClick={toggleNavbar}
          className="block w-full text-center bg-indigo-700 text-white py-2 rounded-t-lg"
        >
          {isOpen ? "Close" : "Questions"}
        </button>
        {isOpen && (
          <div className="grid grid-cols-5 gap-2 p-2">
            {filteredQuestions.map((question, index) => (
              <Link
                key={question.id}
                href={`/survey/${difficulty}/${question.id}`}
                className={`bg-indigo-300 rounded-md p-1 text-center ${
                  currentQuestionId === question.id
                    ? "text-white bg-indigo-700"
                    : ""
                }`}
              >
                {index + 1}
              </Link>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default MobileNavbar;
