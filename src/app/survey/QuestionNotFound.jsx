import Link from "next/link";

export const QuestionNotFound = () => (
  <div className="container mx-auto p-4 m-4 text-center">
    <h1 className="text-xl md:text-2xl font-bold">Question Not Found! 🧐</h1>
    <Link href="/">
      <button className="bg-black text-white font-bold py-2 px-4 mr-6 rounded-md border-2 mt-4 w-full md:w-auto hover:border-black hover:text-black hover:bg-white">
        Start a Survey!
      </button>
    </Link>
  </div>
);
