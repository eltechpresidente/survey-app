import Link from "next/link";
import { IoLogoHtml5, IoLogoCss3, IoLogoJavascript } from "react-icons/io5";
import { FaReact, FaWix, FaLock } from "react-icons/fa";

export default function Page() {
  return (
    <div className="container mx-auto py-4 px-4 rounded">
      <div className="bg-black p-8 rounded-lg shadow-xl">
        <h1 className="text-2xl md:text-3xl font-bold font-mono text-green-400">
          Welcome to HablaCode!
        </h1>
        <p className="text-1xl md:text-2xl font-mono leading-5 text-green-300 mt-4">
          Start your journey here:
        </p>
      </div>

      <div className="mt-8 grid grid-cols-1 md:grid-cols-2 gap-6">
        <Link
          href="/survey/easy/1"
          className="flex items-center justify-between bg-black hover:bg-gray-200 hover:text-black text-white px-6 py-4 rounded-md transition duration-300 ease-in-out"
        >
          <IoLogoHtml5 className="text-3xl text-orange-500" />
          <span className="font-medium">Beginner Course: Easy</span>
        </Link>
        <Link
          href="/survey/medium/11"
          className="flex items-center justify-between bg-black hover:bg-gray-200 hover:text-black text-white px-6 py-4 rounded-md transition duration-300 ease-in-out"
        >
          <IoLogoCss3 className="text-3xl text-blue-500" />
          <span className="font-medium">Intermediate Course: Medium</span>
        </Link>
        <Link
          href="/survey/hard/21"
          className="flex items-center justify-between bg-black hover:bg-gray-200 hover:text-black text-white px-6 py-4 rounded-md transition duration-300 ease-in-out"
        >
          <IoLogoJavascript className="text-3xl text-yellow-500" />
          <span className="font-medium">Advanced Course: Hard</span>
        </Link>
        <div className="flex items-center justify-between bg-gray-300 text-gray-500 px-6 py-4 rounded-md cursor-not-allowed">
          <FaWix className="text-3xl" />
          <span className="font-medium">(Coming Soon)</span>
          <FaLock className="text-2xl" />
        </div>
        <div className="flex items-center justify-between bg-gray-300 text-gray-500 px-6 py-4 rounded-md cursor-not-allowed">
          <FaReact className="text-3xl text-blue-600" />
          <span className="font-medium">(Coming Soon)</span>
          <FaLock className="text-2xl" />
        </div>
      </div>
    </div>
  );
}
